#ifndef ANIMALS_H
#define ANIMALS_H

#include <cmath>
#include <vector>
#include <utility>
#include <string>
#include <map>
#include <memory>
#include <random>
#include <obstacle.h>

using std::vector;
using std::string;
using std::atan2;
using std::hypot;

extern std::default_random_engine generator;
/*!
 * \brief Animal klasa definiująca zwierzę
 */
class Animal
{
    using AnimalPtr=std::unique_ptr<Animal>;
    using animalcollection = std::map<int,AnimalPtr>;
private:
    double x;
    double y;
    double dx;
    double dy;
    double max_v;
    double vision_angle;
    double vision_radius;
    double alignment_weight;
    double cohesion_weight;
    double separation_weight;
    double minimal_distance;
    double obstacle_weight;
    double food_weight;
    double enemy_weight;
    double max_acc;
    double sigma;
    std::normal_distribution<double> distribution;
    void actualizeAlignment(animalcollection const & vec);
    void actualizeCohesion(animalcollection const & vec);
    void actualizeSeparation(animalcollection const & vec);
    void actualizeObstacleInfluence(obstacleCollection const & obst);
    void actualizeFood(animalcollection const & vec);
    void actualizeEnemy(animalcollection const & vec);
protected:
    std::vector<string> foodnames;
    std::vector<string> predatornames;
public:
    /*!
     * \brief Animal konstruktor
     * \param _x współrzędna X położenia początkowego
     * \param _y współrzędna Y położenia początkowego
     * \param _theta orientacja początkowa
     * \param _v prędkość początkowa
     * \param _max_v maksymalna prędkość
     * \param _vision_angle kąt widzenia
     * \param _vision_radius zasięg widzenia
     * \param _alignment_weight
     * \param _cohesion_weight
     * \param _separation_weigh waga separacji
     * \param _minimal_distance minimalny dystans pomiędzy osobnikami
     * \param _obstacle_weight waga przszkody
     * \param _food_weight waga jedzenia
     * \param _enemy_weight waga zagrożenia
     * \param _normal_v domyślna prędkość
     * \param _sigma
     */
    Animal(double _x, double _y, double _theta, double _v, double _max_v,
           double _vision_angle,
           double _vision_radius, double _alignment_weight,
           double _cohesion_weight,
           double _separation_weigh,
           double _minimal_distance,
           double _obstacle_weight,
           double _food_weight,
           double _enemy_weight,
           double _normal_v, double _sigma);
    Animal() : x(0.0), y(0.0), dx(0.0), dy(0.0), max_v(0.0),
        vision_angle(0.0), vision_radius(0.0),
        alignment_weight(0.0), cohesion_weight(0.0),
        separation_weight(0.0), minimal_distance(0.0),
        obstacle_weight(0.0),
        food_weight(0.0),
        enemy_weight(0.0),
        max_acc(0.0), sigma(1.0), distribution(0.0,1.0), foodnames(),
        predatornames()
    {}
    Animal(const Animal &) = default;
    virtual ~Animal() = default;
    /*!
     * \brief getX Zwraca współrzędną X położenia
     * \return współrzędna X położenia
     */
    double getX() const {return x;}
    /*!
     * \brief getY Zwraca współrzędną Y położenia
     * \return współrzędna Y położenia
     */
    double getY() const {return y;}
    /*!
     * \brief getdX Zwraca prędkosć po współrzędnej X
     * \return prędkosć po współrzędnej X
     */
    double getdX() const {return dx;}
    /*!
     * \brief getdY Zwraca prędkosć po współrzędnej Y
     * \return prędkosć po współrzędnej Y
     */
    double getdY() const {return dy;}
    /*!
     * \brief getTheta Zwraca orientację zwierzęcia
     * \return orientację zwierzęcia
     */
    double getTheta() const;
    /*!
     * \brief getV zwraca prędkość zwierzęcia
     * \return zwraca prędkość
     */
    double getV() const {return hypot(dx,dy);}
    /*!
     * \brief getVisionAngle zwraca kąt widzenia zwierzęcia
     * \return kąt widzenia zwierzęcia
     */
    double getVisionAngle() const {return vision_angle;}
    /*!
     * \brief getVisionRadius zwraca zasięg widzenia zwierzęcia
     * \return zasięg widzenia zwierzęcia
     */
    double getVisionRadius() const {return vision_radius;}
    /*!
     * \brief setX ustawia nowe położenie na osi X
     * \param _x nowe położenie na osi X
     */
    void setX(double _x) {x=_x;}
    /*!
     * \brief setY ustawia nowe położenie na osi Y
     * \param _y nowe położenie na osi Y
     */
    void setY(double _y) {y=_y;}
    /*!
     * \brief isInViewRadius sprawdza czy punkt znajduje się w zasięgu widzenia
     * \param px współrzędna x
     * \param py współrzędna y
     * \return True jeśli znajduję się w polu widzenia
     */
    bool isInViewRadius(double px, double py) const;
    bool isInMinimalRadius(double px, double py) const;
    double angleBetween(double px, double py) const;
    bool isInViewAngular(double angleBetween) const;
    void actualize(const animalcollection &mytype, const obstacleCollection &obst, const animalcollection &victimtype, const animalcollection &predatortype, const animalcollection &othertype);
    void limitVelocity();
    void limitAcc(double prvV);
    /*!
     * \brief animalType zwraca nazwę zwierzęcia
     * \return nazwę zwierzęcia
     */
    virtual string animalType() const {return string("Animal");}
    /*!
     * \brief isFood sprawdza czy dane zwierze jest jedzeniem
     * \param name nazwa zwierzęcia
     * \return True jeśli jest jedzeniem
     */
    bool isFood(string name) const;
    /*!
     * \brief isPredator sprawdza czy dane zwierze jest drapierznikiem
     * \param name nazwa zwierzęcia
     * \return True jeśli jest drapierznikiem
     */
    bool isPredator(string name) const;

    /*!
     * \brief getAttribute zwraca listę atrybutów opisującą dany typ rasy
     * \return mapa atrybutów
     */
    std::map<std::string,double> getAttribute() const;
    /*!
     * \brief setAttribute ustawia atrybuty
     * \param attribute mapa atrybutów
     */
    void setAttribute(std::map<std::string,double> attribute);
};

template <typename T,typename U>
std::pair<T,U> operator+(const std::pair<T,U> & l,const std::pair<T,U> & r) {
    return {l.first+r.first,l.second+r.second};
}

/*!
 * \brief Velociraptor klasa dziedzyczy po Animal definuje klasę zwierząt Verociraptorów
 */
class Velociraptor: public Animal
{
public:
    Velociraptor(double _x, double _y, double _theta, double _v);
    Velociraptor() = default;
    Velociraptor(const Velociraptor &) = default;
    Velociraptor(Velociraptor &&) = default;
    ~Velociraptor() = default;
    string animalType() const override;
};

/*!
 * \brief Lion klasa dziedzyczy po Animal definuje klasę zwierząt Lwów
 */
class Lion: public Animal
{
public:
    Lion(double _x, double _y, double _theta, double _v);
    Lion() = default;
    Lion(const Lion &) = default;
    Lion(Lion &&) = default;
    ~Lion() = default;
    string animalType() const override;
};

/*!
 * \brief Deer klasa dziedzyczy po Animal definuje klasę zwierząt Jeleni
 */
class Deer: public Animal
{
public:
    Deer(double _x, double _y, double _theta, double _v);
    Deer() = default;
    Deer(const Deer &) = default;
    Deer(Deer &&) = default;
    ~Deer() = default;
    string animalType() const override;
};

#endif // ANIMALS_H
