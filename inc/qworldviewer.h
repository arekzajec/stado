#ifndef QWORLDVIEWER_H
#define QWORLDVIEWER_H
#include <world.h>
#include <QWidget>
#include <map>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <qanimals.h>
#include <memory>
#include <obstacle.h>

/*!
 * \brief QWorldScene klasa defiuniująca scenę, implementuję obsługę myszy
 */
class QWorldScene: public QGraphicsScene
{
public:
    QWorldScene(QWidget *&parent);

protected:
    /*!
     * \brief mousePressEvent obsługa zdarzenia naciśnięcia lewego klawisza myszy
     */
    void mousePressEvent(QGraphicsSceneMouseEvent*);
    /*!
     * \brief mouseMoveEvent obsługa zdarzenia przejechania myszą
     */
    void mouseMoveEvent(QGraphicsSceneMouseEvent*);
    /*!
     * \brief mouseReleaseEvent obsługa zdarzenia puszczenia lewego klawisza myszy
     */
    void mouseReleaseEvent(QGraphicsSceneMouseEvent*);

private:
    bool pressed;
    QPointF oldMousePos;
};

/*!
 * \brief QWorldViewer definuję sposób wyświetlania świata
 */
class QWorldViewer
{
    using qanimalcollection = std::map<int,QAnimal*>;
    using qanimalviewcollection = std::map<int,QAnimalView*>;
public:

    QWorldViewer(QWidget *parent,
                 const WorldInterface &world,
                 std::shared_ptr<Observer>observer);
    QWorldViewer(QWorldViewer&) = default;
    ~QWorldViewer();
    QWorldViewer &operator=(const QWorldViewer&) = default;
    /*!
     * \brief getScene zwraca wskaźnik na scene
     * \return wskażnik na scenę
     */
    QGraphicsScene *getScene();
    /*!
     * \brief updateScene aktualizuję scene
     */
    void updateScene();
    /*!
     * \brief updateAnimalItems aktualizuję listę zwierząt na scenie
     */
    void updateAnimalItems();

public slots:
    /*!
     * \brief refreshAnimalItems powoduję aktualizację listy zwierząt na scenie
     */
    void refreshAnimalItems();

private:
    void updateAnimals();
    void addAnimal(const std::pair<const int,std::unique_ptr<Animal>>&animal);
    void newObstacles(ObstacleContainer const & _obstacles);
    void updateObstacleItems();
    void reloadAnimals();
private:
    QWorldScene *scene;
    const WorldInterface &world;
    std::shared_ptr<Observer> observer;
    qanimalcollection qanimals;
    qanimalviewcollection qanimalsview;
    ObstacleContainer obstacles;
    int idUpdate;
    int idReload;
};

#endif // QWORLDVIEWER_H
