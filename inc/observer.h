#ifndef OBSERVER_H
#define OBSERVER_H
#include <functional>
#include <map>

using handler = std::function<void ()>;

/*! @brief Observer4Visualization interfejs klasy Observer
 *
 *  Jest to interfejs udostępniany dla wizualizacji
 */
class Observer4Visualization {
public:
    virtual ~Observer4Visualization() = default;
    /*!
     * \brief subscribeUpdate jest to metoda która powoduję dodanie funkcji do listy uruchamianych
     *  po wykonaniu metody notifyUpdate z klasy Observer4World
     * \param hnd Wskaźnik na funkcję
     * \return identyfikator dodanej funkcji, potrzebyny w celu wyrejstrowania funkcji
     */
    virtual int subscribeUpdate(handler hnd) = 0;

    /*!
     * \brief subscribeReload jest to metoda która powoduję dodanie funkcji do listy uruchamianych po wykonaniu metody notifyReload z klasy Observer4World
     * \param hnd Wskaźnik na funkcję
     * \return identyfikator dodanej funkcji, potrzebyny w celu wyrejstrowania funkcji
     */
    virtual int subscribeReload(handler hnd) = 0;
    /*!
     * \brief unsubscribeUpdate powoduję wyrejstrowanie funkcji z listy update powiadamianych
     * \param id identyfikator funkcji
     */
    virtual void unsubscribeUpdate(int id) = 0;
    /*!
     * \brief unsubscribeReload powoduję wyrejstrowanie funkcji z listy reload powiadamianych
     * \param id identyfikator funkcji
     */
    virtual void unsubscribeReload(int id) = 0;
};

/*!
 * \brief Observer4World interfejs klasy Observer
 *
 * Jest to interfejs udostępniony dla klasy World
 */
class Observer4World {
public:
    virtual ~Observer4World() = default;
    /*!
     * \brief notify Powoduję uruchomienie wszystkich funkcji które są na liście update subskrybentów
     */
    virtual void notifyUpdate() = 0 ;
    /*!
     * \brief notify Powoduję uruchomienie wszystkich funkcji które są na liście reload subskrybentów
     */
    virtual void notifyReload() = 0 ;
};

/*!
 * \brief Observer klasa implementująca wzorzec projektowy obserwatora
 *
 * Obserwator powoduję uruchomienie wszystkich subskrybowanych funkcji po wykonaniu metody powiadamiającej notify
 */
class Observer : public Observer4Visualization, public Observer4World {
    std::map<int, handler> hnds_update;
    std::map<int, handler> hnds_reload;

    int nextid_update;
    int nextid_reload;

public:
    Observer() : hnds_update(),hnds_reload(),nextid_update(0),nextid_reload(0) {}
    Observer(Observer &) = default;
    int subscribeUpdate(handler hnd) override {
        hnds_update[++nextid_update] = hnd;
        return nextid_update;
    }
    void unsubscribeUpdate(int id) override {
        hnds_update.erase(id);
    }
    int subscribeReload(handler hnd) override {
        hnds_reload[++nextid_reload] = hnd;
        return nextid_reload;
    }
    void unsubscribeReload(int id) override {
        hnds_reload.erase(id);
    }
    void notifyUpdate() {
        for (auto & it : hnds_update) {
            it.second();
        }
    }

    void notifyReload() {
        for (auto & it : hnds_reload) {
            it.second();
        }
    }
};

#endif // OBSERVER_H
