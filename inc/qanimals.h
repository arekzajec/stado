#ifndef QANIMALS_H
#define QANIMALS_H

#include <QGraphicsItem>

/*!
 * \brief QAnimal klasa definiująca wygląd zwierzęcia
 */
class QAnimal: public QGraphicsItem
{
private:
    QColor bodyColor;
    QColor eyeColor;

    int size;

protected:
    void drawBody(QPainter *painter);
    void drawEye(QPainter *painter);
public:
    /*!
     * \brief QAnimal konstruktor klasy
     * \param size wielkość
     * \param bodyColor kolor ciała
     * \param eyeColor kolor oczu
     */
    QAnimal(int size, QColor bodyColor, QColor eyeColor);
    /*!
     * \brief boundingRect zwraca obszar w którym znajduję się zwierze
     * \return obszar zawierający zwierze
     */
    QRectF boundingRect() const;
    /*!
     * \brief paint Powoduję odrysowanie zwierzęcia
     * \param painter
     * \param option
     * \param widget
     */
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

};

/*!
 * \brief QAnimalView klasa definiująca wygląd obszaru widzenia zwierzęcia
 */
class QAnimalView: public QGraphicsItem
{
private:
    int size;
    QColor viewColor;
    int viewAngle;
    int viewRange;

private:
    void drawView(QPainter *painter);
public:
    /*!
     * \brief QAnimalView konstruktor klasy
     * \param size rozwiar
     * \param viewColor kolor obszaru widzenia
     * \param viewAngle kąt widznia
     * \param viewRange zasięg widzenia
     */
    QAnimalView(int size, QColor viewColor, int viewAngle, int viewRange);
    /*!
     * \brief boundingRect zwraca prostokąt w którym znajduję się obszar widzenia zwierzęcia
     * \return prostokąt zawierający obszar widzenia
     */
    QRectF boundingRect() const;
    /*!
     * \brief paint Powoduję odrysowanie obszaru widznia
     * \param painter
     * \param option
     * \param widget
     */
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    /*!
     * \brief setViewRad Zadaję zasię oraz kąt widzenia
     * \param viewAngle kąt widzenia
     * \param viewRange zasięg widzenia
     */
    void setViewRad(double viewAngle, double viewRange);

};

/*!
 * \brief QVelociraptor klasa definiująca wygląd Velociraptora
 */
class QVelociraptor: public QAnimal
{
public:
    QVelociraptor():QAnimal(10,Qt::red,Qt::white){}
};

/*!
 * \brief QLion klasa definiująca wygląd Lwa
 */
class QLion: public QAnimal
{
public:
    QLion():QAnimal(12,Qt::yellow,Qt::white){}
};

/*!
 * \brief QDeer klasa definiująca wygląd Łosia
 */
class QDeer: public QAnimal
{
public:
    QDeer():QAnimal(10,Qt::darkYellow,Qt::white){}
};


#endif // QANIMALS_H
