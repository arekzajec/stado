#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qanimals.h>
#include <QGraphicsView>
#include <qworldviewer.h>
#include <QTimer>

namespace Ui {
class MainWindow;
}
/*!
 * \brief MainWindow definiuję wygląd głównego okna aplikacji
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    MainWindow(MainWindow&) = default;
    MainWindow& operator =(const MainWindow&) = default;
    ~MainWindow();
    
private slots:
    void actualizeWorld();

    void on_widget_destroyed();
    void refreshAnimalItems();
    void on_start_button_clicked();

    void on_slower_button_clicked();

    void on_faster_button_clicked();

private:
    Ui::MainWindow *ui;
    std::shared_ptr<Observer> observer;
    World *world;
    QWorldViewer *worldViewer;
    double zoom;
    QTimer *timer;
    int timerInterval;
};

#endif // MAINWINDOW_H
