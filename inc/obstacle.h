#ifndef OBSTACLE_H
#define OBSTACLE_H
#include <vector>
#include <string>
#include <fstream>

/*!
 * \brief Point klasa definiująca punkt
 */
class Point
{
public:
    Point();
    Point(const Point& point);
    Point(double x, double y);
    /*!
     * \brief set ustawia wartości x oraz y
     * \param x
     * \param y
     */
    void set(double x, double y);
    /*!
     * \brief setX ustawia wartość X
     * \param x
     */
    void setX(double x);
    /*!
     * \brief setY ustawia wartość y
     * \param y
     */
    void setY(double y);
    /*!
     * \brief getX zwraca wartość x
     * \return wartość x
     */
    double getX() const;
    /*!
     * \brief getY zwraca wartość y
     * \return wartość y
     */
    double getY() const;
    /*!
     * \brief len oblicza odległość pomiędzy punktami
     * \param point punkt do którego mierzymy odległość
     * \return odległość
     */
    double len(const Point& point) const;
    friend std::ostream& operator<<(std::ostream& out, const Point& point);
    friend std::istream& operator>>(std::istream& in, Point& point);

private:
    double _x;
    double _y;
};




using pointCollection=std::vector<Point>;
/*!
 * \brief Obstacle klasa definująca przeszkodę
 */
class Obstacle
{
public:
    Obstacle();
    /*!
     * \brief isInside metoda sprawdzająca czy punkt znajduję się wewnątrz przeszkody
     * \param point punkt do sprawdzenia
     * \return true jeśli jest wewnątrz jeśli inaczej to false
     */
    bool isInside(const Point& point) const;
    /*!
     * \brief addVertice dodaje wieszchołek do przeszkody
     * \param point dodawany wieszchołek
     */
    void addVertice(const Point& point);
    /*!
     * \brief getVertices Zwraca listę wieszchołków
     * \return lista wieszchołków
     */
    const pointCollection& getVertices() const;
    friend std::ostream& operator<<(std::ostream& out, const Obstacle& obstacle);
    friend std::istream& operator>>(std::istream& in, Obstacle& obstacle);

private:
    bool isOnLeft(const Point& point, const Point& A, const Point& B) const;

private:
    pointCollection vertices;

};



using obstacleCollection = std::vector<Obstacle>;
/*!
 * \brief ObstacleContainer klasa definująca kontener przeszkód
 */
class ObstacleContainer
{
public:
    ObstacleContainer();
    /*!
     * \brief save powoduję zapis wszystkich przeszkód do pliku
     * \param fileName nazwa pliku
     * \return true jeśli zapis się powiódł
     */
    bool save(std::string fileName) const;
    /*!
     * \brief open otwiera plik z zapisanymi przeszkodami
     * \param fileName nazwa pliku
     * \return true jeśli otwarcie przebiegło pomyślnie
     */
    bool open(std::string fileName);
    /*!
     * \brief addObstacle dodaję przeszkodę do kontenera
     * \param obstacle przeszkoda
     */
    void addObstacle(const Obstacle& obstacle);
    /*!
     * \brief addObstacles dodaję przeszkody do kontenera
     * \param obstacles kontener przeszkód
     */
    void addObstacles(const ObstacleContainer& obstacles);
    /*!
     * \brief getObstacles zwraca listę przeszkód
     * \return listę przeszkód
     */
    const obstacleCollection& getObstacles() const;
    friend std::ostream& operator<<(std::ostream& out, const ObstacleContainer& obstacles);
    friend std::istream& operator>>(std::istream& in, ObstacleContainer& obstacles);

private:
    obstacleCollection _obstacles;
};

#endif // OBSTACLE_H
