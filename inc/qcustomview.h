#ifndef QCUSTOMVIEW_H
#define QCUSTOMVIEW_H
#include <QGraphicsView>
#include <QWheelEvent>

/*!
 * \brief QCustomView klasa definiująca sposób wświetlania sceny
 */
class QCustomView : public QGraphicsView
{
public:
    QCustomView(QWidget *&parent);

protected:
    /*!
     * \brief wheelEvent obsługa rolki myszy potrzebna do zoomowania sceny
     * \param event
     */
    void wheelEvent(QWheelEvent *event);

private:
    double zoom;
};

#endif // QCUSTOMVIEW_H
