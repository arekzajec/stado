#ifndef WORLD_H
#define WORLD_H

#include "animals.h"
#include <vector>
#include <algorithm>
#include <map>
#include "observer.h"
#include <memory>
#include <chrono>
#include <thread>
#include "obstacle.h"

using AnimalPtr=std::unique_ptr<Animal>;
using animalcollection = std::map<int,AnimalPtr>;
using std::shared_ptr;

/*!
 * \brief WorldInterface interfejs klasy World
 */
class WorldInterface {
public:
    virtual ~WorldInterface() = default;
    /*!
     * \brief getAnimalsPtr zwraca wskaźnik na kontener zwierząt
     * \return wskaźnik na kontener zwierząt
     */
    virtual animalcollection  *getAnimalsPtr() = 0;
    /*!
     * \brief getAnimals zwraca const referencję do kontenera zwierząt
     * \return const referencję do kontenera zwierząt
     */
    virtual animalcollection const & getAnimals() const = 0;
    /*!
     * \brief getObstacles zwraca const referencję do kontenera przeszkód
     * \return const referencję do kontenera przeszkód
     */
    virtual ObstacleContainer const & getObstacles() const =0;
};

/*!
 * \brief World klasa definująca działanie symulatora świata
 */
class World : public WorldInterface {
private:
    animalcollection Animals;
    int highestAnimalIndex;
    animalcollection temporaryAnimalsMyType;
    animalcollection temporaryAnimalsVictimType;
    animalcollection temporaryAnimalsPredatorType;
    animalcollection temporaryAnimalsOtherType;
    shared_ptr<Observer> observer;
    ObstacleContainer obstacles;
public:
    World(shared_ptr<Observer> _observer): Animals(), highestAnimalIndex(0),
        temporaryAnimalsMyType(), temporaryAnimalsVictimType(), temporaryAnimalsPredatorType(), temporaryAnimalsOtherType(), observer(_observer), obstacles() {}
    animalcollection *getAnimalsPtr();
    animalcollection const &getAnimals() const override;
    ObstacleContainer const & getObstacles() const override;
    /*!
     * \brief friendlyAnimalsInView zwraca listę zwierząt zaprzyjaźnionych w polu widzenia
     * \param A zwierze
     * \return liste zwierząt zaprzyjaźnionych
     */
    animalcollection friendlyAnimalsInView(Animal const & A) const;
    /*!
     * \brief victimAnimalsInView zwraca listę ofiar w polu widzenia
     * \param A zwierze
     * \return listę ofiar w polu widzenia
     */
    animalcollection victimAnimalsInView(Animal const & A) const;
    /*!
     * \brief predatorAnimalsInView zwraca listę drapierzników w polu widzenia
     * \param A zwierze
     * \return listę drapierzników w polu widzenia
     */
    animalcollection predatorAnimalsInView(Animal const & A) const;
    animalcollection otherAnimalsInView(Animal const & A) const;
    /*!
     * \brief addAnimal dodaję zwierzę do świata
     * \param A zwierze
     */
    void addAnimal(AnimalPtr A);
    /*!
     * \brief addObstacle dodaję przeszkodę do świata
     * \param obstacle przeszkoda
     */
    void addObstacle(const Obstacle & obstacle);
    /*!
     * \brief addObstacle dodaję listę przeszkód do świata
     * \param obstacles kontner przeszkód
     */
    void addObstacle(const ObstacleContainer & obstacles);
    /*!
     * \brief actualize aktualizuję stan świata
     */
    void actualize();
    void go();
};

#endif // WORLD_H
