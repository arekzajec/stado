#ifndef QANIMALSETTINGS_H
#define QANIMALSETTINGS_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QTabWidget>
#include <QDoubleSpinBox>
#include <vector>
#include <map>
#include <animals.h>
#include <world.h>
#include <set>

/*!
 * \brief QAnimalSettingsTab definuję wygląd zakładki z ustawieniami dla danej razy zwierząt
 */
class QAnimalSettingsTab : public QWidget
{
    Q_OBJECT
public:
    explicit QAnimalSettingsTab(QWidget *parent = 0, animalcollection *animals=nullptr, string animalType="");
    QAnimalSettingsTab(const QAnimalSettingsTab&) = delete;
    QAnimalSettingsTab operator =(const QAnimalSettingsTab&) = delete;
private:
    void updateAttribute(const Animal & animal);

protected slots:
    void changeData();

signals:
    void changeAnimlData();

private:
    QGridLayout lay;
    std::map<std::string,QDoubleSpinBox*> spinboxs;
    animalcollection *animals;
    std::string animalType;
};

/*!
 * \brief QAnimalSettings definiuje wygląd oraz zachowanie ustawień parametrów ras
 */
class QAnimalSettings : public QWidget
{
    Q_OBJECT
public:
    explicit QAnimalSettings(QWidget *parent = 0);
    void setAnimals(animalcollection* animals);
    QAnimalSettings(const QAnimalSettings&) = delete;
    QAnimalSettings operator =(const QAnimalSettings&) = delete;
signals:
    void changeAnimlData();

public slots:
    void changeData();

private:
    animalcollection *animals;
    QTabWidget tabWidget;
    std::map<std::string,QAnimalSettingsTab*> tabs;
};


#endif // QANIMALSETTINGS_H
