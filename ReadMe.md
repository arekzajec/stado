Stadooo - Symulator Stada
===========================
#### Komputerowe przetwarzanie wiedzy
Zagadnienie poruszania się zwierząt w stadzie ciekawi ludzie od bardzo
dawna. Patrząc na ptaki lecące po niebie w kluczu, ławice ryb morskich albo
nawet stada saren mozna odnieść wrażenie, że zwierzętami tymi kieruje jakaś 
wspólna świadomość. Ze samo stado, ławica czy klucz jest samodzielnym podmiotem
 mogącym decydować o ruchu poszczególnych zwierząt i synchronizującym
go. Wrazenie to jest źródłem pewnego dysonansu poznawczego, ponieważ
nie jesteśmy w stanie zlokalizować miejsca tej świadomości w samej strukturze
stada - widzimy jedynie zbiór pojedynczych jednostek. Okazuje się jednak, ze
nasze początkowe przeświadczenie o scentralizownym, skomplikowanym systemie
sterowanie są błędne, a zasady rządzące zachowaniem stada mozna modelować
 prostymi regułami.

Aplikacja pozwala na badanie i wizualizację wpływu
poszczególnych reguł na zachowanie stada. 
Aplikacja ta została napisana w języku C++14 przy wykorzystaniu kompilatora GNU gcc 4.9.2, biblioteki Qt
5.4.1 oraz Qt Creator IDE 3.1.1.
Aplikacja udostępnia interfejs uzytkownika w którym można edytować parametry stada kazdego z dostępnych gatunków
### Zależności:
> build-essential <br />
> libqt4-dev

### Budowanie oraz uruchomienie:
> qmake <br />
> make <br />
> ./Stadooo 
### Autorzy:
Damian Barański <br />
Arkadiusz Mielczarek

### Licencja
GNU General Public License v3.0
