#include "inc/obstacle.h"
#include <cmath>
#include <iostream>
Obstacle::Obstacle():
    vertices()

{
}

bool Obstacle::isInside(const Point &point) const
{
    //Sprawdzam czy punkt znajduję się w przeskodzie
    //Poruszam się w lewo od punktu i sprawdzam ile krawędzi przecina
    //Parzysta ilość oznacza punkt na zewnątrz, nie parzysta wewnątrz
    int intersections=0;

    for(unsigned int i=0; i<vertices.size()-1;i++)
    {
        if(isOnLeft(point,vertices[i],vertices[i+1]))
            intersections++;
    }
    if(isOnLeft(point,vertices[vertices.size()-1],vertices[0]))
        intersections++;

    if(intersections%2==0)
        return false;
    else
        return true;
}

void Obstacle::addVertice(const Point &point)
{
    vertices.push_back(point);
}

const pointCollection &Obstacle::getVertices() const
{
    return vertices;
}

std::istream &operator>>(std::istream &in, Obstacle &obstacle)
{
    char character;
    Point point;

    in >> character;
    while(character!=')' && !in.eof() && !in.fail())
    {
        std::cout << character <<std::endl;
        in.putback(character);
        in >> point;
        in >> character;
        obstacle.addVertice(point);
    }
    return in;
}

bool Obstacle::isOnLeft(const Point &point, const Point &A, const Point &B) const
{
    double x;
    //Jeśli odcinek jest poziomy
    if(A.getY()==B.getY())
        return false;

    if((point.getY()<A.getY() && point.getY()<B.getY()) || (point.getY()>A.getY() && point.getY()>B.getY()))
        return false;

    x=(point.getY()-A.getY())*(B.getX()-A.getX())/(B.getY()-A.getY())+A.getX();
    if(point.getX()<x)
        return true;
    else
        return false;
}


Point::Point():
    _x(0),_y(0)
{}

Point::Point(const Point &point):
    _x(point.getX()),_y(point.getY())
{}

Point::Point(double x, double y):
    _x(x),_y(y)
{}

void Point::set(double x, double y)
{
    _x=x;
    _y=y;
}

void Point::setX(double x)
{
    _x=x;
}

void Point::setY(double y)
{
    _y=y;
}

double Point::getX() const
{
    return _x;
}

double Point::getY() const
{
    return _y;
}

double Point::len(const Point &point) const
{
    return sqrt(pow(_x-point.getX(),2)+pow(_y-point.getY(),2));
}

std::istream &operator>>(std::istream &in, Point &point)
{
    char character;
    //Sprawczam czy otwarcie jest nawiasem
    in >> character;
    double x, y;
    in >> x;
    in >> character;
    in >> y;
    in >> character;

    point.set(x,y);
    return in;
}



ObstacleContainer::ObstacleContainer():
    _obstacles()
{
}

bool ObstacleContainer::save(std::string fileName) const
{
    std::ofstream outFile(fileName.c_str());
    if(!outFile.is_open())
        return false;
    outFile << this;
    return true;
}

bool ObstacleContainer::open(std::string fileName)
{
    std::ifstream inFile(fileName.c_str());
    if(!inFile.is_open())
        return false;

    inFile >> *this;
    return false;
}

void ObstacleContainer::addObstacle(const Obstacle &obstacle)
{
    _obstacles.push_back(obstacle);
}

void ObstacleContainer::addObstacles(const ObstacleContainer &obstacles)
{
    for(auto obstacle : obstacles.getObstacles())
        _obstacles.push_back(obstacle);
}

const obstacleCollection &ObstacleContainer::getObstacles() const
{
    return _obstacles;
}

std::istream &operator>>(std::istream &in, ObstacleContainer &obstacles)
{
    char character;
    Obstacle obstacle;
    in >> character;

    in >> character;
    while(character!=')'  && !in.eof() && !in.fail())
    {
        in.putback(character);
        in >> obstacle;
        in >> character;
        obstacles.addObstacle(obstacle);
    }
    return in;
}

std::ostream &operator<<(std::ostream &out, const ObstacleContainer &obstacles)
{
    for(auto obstacle : obstacles._obstacles)
    {
        out << obstacle << std::endl;
    }
    return out;
}


std::ostream &operator<<(std::ostream &out, const Obstacle &obstacle)
{
    out << "(";
    for(auto point : obstacle.vertices)
    {
        out << point;
    }
    out << ")";
    return out;
}



std::ostream &operator<<(std::ostream &out, const Point &point)
{
    out << "(" << point.getX() << "," << point.getY() << ")";
    return out;
}
