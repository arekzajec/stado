#include "world.h"

using std::copy;

animalcollection World::friendlyAnimalsInView(Animal const & A) const {
    animalcollection output;
    for (auto & it : Animals) {
        if (A.isInViewRadius(it.second->getX(),it.second->getY()) &&
                (A.isInViewAngular(A.angleBetween(it.second->getX(),it.second->getY())) ||
                 A.isInMinimalRadius(it.second->getX(),it.second->getY()) ) &&
                &A != it.second.get() && A.animalType()==it.second->animalType())
        {
            output[it.first] =  std::make_unique<Animal>(Animal(*(it.second)));
        }
    }
    return output;
}

animalcollection World::victimAnimalsInView(Animal const & A) const {
    animalcollection output;
    for (auto & it : Animals) {
        if (A.isInViewRadius(it.second->getX(),it.second->getY()) &&
                (A.isInViewAngular(A.angleBetween(it.second->getX(),it.second->getY())) ||
                 A.isInMinimalRadius(it.second->getX(),it.second->getY()) ) &&
                &A != it.second.get() && A.isFood(it.second->animalType()) )
        {
            output[it.first] =  std::make_unique<Animal>(Animal(*(it.second)));
        }
    }
    return output;
}

animalcollection World::predatorAnimalsInView(Animal const & A) const {
    animalcollection output;
    for (auto & it : Animals) {
        if (A.isInViewRadius(it.second->getX(),it.second->getY()) &&
                (A.isInViewAngular(A.angleBetween(it.second->getX(),it.second->getY())) ||
                 A.isInMinimalRadius(it.second->getX(),it.second->getY()) ) &&
                &A != it.second.get() && A.isPredator(it.second->animalType()) )
        {
            output[it.first] =  std::make_unique<Animal>(Animal(*(it.second)));
        }
    }
    return output;
}

animalcollection World::otherAnimalsInView(Animal const & A) const {
    animalcollection output;
    for (auto & it : Animals) {
        if (A.isInViewRadius(it.second->getX(),it.second->getY()) &&
                (A.isInViewAngular(A.angleBetween(it.second->getX(),it.second->getY())) ||
                 A.isInMinimalRadius(it.second->getX(),it.second->getY()) ) &&
                &A != it.second.get() && A.animalType()!=it.second->animalType() &&
                !A.isFood(it.second->animalType()) && !A.isPredator(it.second->animalType()) )
        {
            output[it.first] =  std::make_unique<Animal>(Animal(*(it.second)));
        }
    }
    return output;
}

void World::addAnimal(AnimalPtr A) {
    Animals[++highestAnimalIndex] = std::move(A);
}

animalcollection *World::getAnimalsPtr() {
    return &Animals;
}

const animalcollection &World::getAnimals() const
{
    return Animals;
}

void World::addObstacle(const Obstacle & obstacle) {
    obstacles.addObstacle(obstacle);
}

void World::addObstacle(const ObstacleContainer &obstacles)
{
    this->obstacles.addObstacles(obstacles);
}

ObstacleContainer const & World::getObstacles() const {
    return obstacles;
}

double eps=5.0;

void World::actualize() {
    obstacleCollection temporaryObstPoints;
    for(auto & it : Animals) {
        temporaryAnimalsMyType = friendlyAnimalsInView(*(it.second));
        temporaryAnimalsVictimType = victimAnimalsInView(*(it.second));
        temporaryAnimalsPredatorType = predatorAnimalsInView(*(it.second));
        temporaryAnimalsOtherType =otherAnimalsInView(*(it.second));
        temporaryObstPoints = getObstacles().getObstacles();
        it.second->actualize(temporaryAnimalsMyType, temporaryObstPoints, temporaryAnimalsVictimType, temporaryAnimalsPredatorType, temporaryAnimalsOtherType);
    }
    for(auto & it : Animals) {
        it.second->setX(it.second->getX() + it.second->getV()*cos(it.second->getTheta()));
        it.second->setY(it.second->getY() + it.second->getV()*sin(it.second->getTheta()));
    }
    std::vector<int> eraseindex;
    for (auto it1 = Animals.begin();it1!=Animals.end();++it1) {
        for (auto it2 = Animals.begin();it2!=Animals.end();++it2) {
            if (abs(it1->second->getX()-it2->second->getX()) < eps &&
                    abs(it1->second->getY()-it2->second->getY()) < eps &&
                    it1->second->isFood(it2->second->animalType()) ) {
                eraseindex.push_back(it2->first);
            }
        }
    }
    for (auto & it : eraseindex) {
        Animals.erase(it);
        observer->notifyReload();
    }
    observer->notifyUpdate();
}

void World::go() {
    while(true) {
        actualize();
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}
