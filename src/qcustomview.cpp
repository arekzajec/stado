#include "qcustomview.h"


QCustomView::QCustomView(QWidget *&parent): QGraphicsView(parent), zoom(1){}

void QCustomView::wheelEvent(QWheelEvent *event)
{
    (void) event;
    if(event->delta()<0)
    {
        scale(0.95,0.95);
        zoom=0.95*zoom;
    }
    else if(zoom<=1)
    {
        scale(1.05,1.05);
        zoom=1.05*zoom;
    }
}
