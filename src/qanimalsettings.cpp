#include "inc/qanimalsettings.h"

QAnimalSettings::QAnimalSettings(QWidget *parent) :
    QWidget(parent),animals(nullptr),tabWidget(this),tabs()
{

}

void QAnimalSettings::setAnimals(animalcollection *animals)
{
    this->animals=animals;
    std::set<std::string> AnimalNames;
    for(const auto &animal : (*animals))
    {
        AnimalNames.insert(animal.second->animalType());
    }
    QHBoxLayout *lay=new QHBoxLayout(this);
    lay->addWidget(&tabWidget);
    setLayout(lay);
    for(auto animalName : AnimalNames)
    {
      tabs[animalName]=new QAnimalSettingsTab(this,animals,animalName);
      connect(tabs[animalName],SIGNAL(changeAnimlData()),this,SLOT(changeData()));
      tabWidget.addTab(tabs[animalName],animalName.c_str());
    }
}


QAnimalSettingsTab::QAnimalSettingsTab(QWidget *parent, animalcollection *animals, string animalType):
    QWidget(parent),lay(),spinboxs(),animals(nullptr),animalType()
{
    setLayout(&lay);
    this->animalType=animalType;
    this->animals=animals;
    for(const auto &animal : *animals)
    {
        (void) animal;
        if(animal.second->animalType()==animalType)
        {
            updateAttribute(*(animal.second));
            break;
        }
    }

}

void QAnimalSettingsTab::updateAttribute(const Animal &animal)
{
    int i=0;
    for(auto attribute : animal.getAttribute())
    {
        spinboxs[attribute.first]=new QDoubleSpinBox(this);
        spinboxs[attribute.first]->setMaximum(1000);
        spinboxs[attribute.first]->setValue(attribute.second);

        connect(spinboxs[attribute.first],SIGNAL(valueChanged(double)),this,SLOT(changeData()));

        lay.addWidget(new QLabel(attribute.first.c_str(),this),i,0);
        lay.addWidget(spinboxs[attribute.first],i,1);
        i++;
    }
}

void QAnimalSettingsTab::changeData()
{
    std::map<std::string,double> attribute;
    for(auto data : spinboxs)
    {
        attribute[data.first]=data.second->value();
    }
    for(auto &animal : *animals)
    {
        if(animal.second->animalType()==animalType)
            animal.second->setAttribute(attribute);
    }
    emit changeAnimlData();
}

void QAnimalSettings::changeData()
{
    emit changeAnimlData();
}
