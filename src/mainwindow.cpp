#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <memory>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    observer(new Observer()),
    world(new World(observer)),
    worldViewer(new QWorldViewer(this,*world,observer)),
    zoom(1), timer(new QTimer()),timerInterval(100)
{
    ui->setupUi(this);
    ui->graphicsView->setScene(worldViewer->getScene());

    Obstacle obstacle1;
    obstacle1.addVertice(Point(300-50-300,300+20+50));
    obstacle1.addVertice(Point(300-50-300,300-20+50));
    obstacle1.addVertice(Point(300-10-300,300-20+50));
    obstacle1.addVertice(Point(300-10-300,300+20+50));

    Obstacle obstacle2;
    obstacle2.addVertice(Point(300-20,300+20+50));
    obstacle2.addVertice(Point(300-20,300-20+50));
    obstacle2.addVertice(Point(300+20,300-20+50));
    obstacle2.addVertice(Point(300+20,300+20+50));

    /* world->addAnimal(std::make_unique<Velociraptor>(Velociraptor(0,50,0,2)));
    world->addAnimal(std::make_unique<Velociraptor>(Velociraptor(40,80,0,1)));
    world->addAnimal(std::make_unique<Velociraptor>(Velociraptor(80,73,0,1)));
    world->addAnimal(std::make_unique<Velociraptor>(Velociraptor(120,60,0,1.5)));
    world->addAnimal(std::make_unique<Velociraptor>(Velociraptor(160,40,0,1)));
    world->addAnimal(std::make_unique<Velociraptor>(Velociraptor(200,85,0,0.1)));
    world->addObstacle(obstacle1);
    world->addObstacle(obstacle2);
    world->addAnimal(std::make_unique<Lion>(Lion(-400,600,0,4)));
    world->addAnimal(std::make_unique<Deer>(Deer(100,100,0,0)));*/

    for(int i=0; i<10;i++)
    {
        world->addAnimal(std::make_unique<Velociraptor>(Velociraptor(rand()%1000,rand()%1000,0,0)));
    }
    for(int i=0; i<30;i++)
    {
        world->addAnimal(std::make_unique<Lion>(Lion(rand()%1000+500,rand()%1000+500,rand()%1000,(rand()%100)/10.0)));
    }
    for(int i=0; i<30;i++)
    {
        world->addAnimal(std::make_unique<Deer>(Deer(rand()%1000+1000,rand()%1000+1000,rand()%1000,(rand()%100)/10.0)));
    }

    worldViewer->updateScene();
    world->addAnimal(std::make_unique<Velociraptor>(Velociraptor(300,-30,M_PI/2.5,1)));
    //    world->addAnimal(std::make_unique<Velociraptor>(Velociraptor(0,300+50,0,1,0)));
    //    world->addAnimal(std::make_unique<Velociraptor>(Velociraptor(0,300-50,0,1,0)));

    worldViewer->updateScene();
    connect(timer,SIGNAL(timeout()),this,SLOT(actualizeWorld()));

    ui->widget->setAnimals(world->getAnimalsPtr());
    connect(ui->widget,SIGNAL(changeAnimlData()),this,SLOT(refreshAnimalItems()));;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::actualizeWorld()
{
    world->actualize();
    ui->graphicsView->scale(1.00001,1.00001);
    ui->graphicsView->scale(1/1.00001,1/1.00001);
    static int i=0;
    qDebug() << i++;
}

void MainWindow::on_widget_destroyed()
{

}

void MainWindow::refreshAnimalItems()
{
    worldViewer->refreshAnimalItems();

}

void MainWindow::on_start_button_clicked()
{
    if(timer->isActive())
    {
        timer->stop();
        ui->start_button->setText("Start");
    }
    else
    {
        ui->start_button->setText("Pauza");
        timer->start(timerInterval);
    }

}

void MainWindow::on_slower_button_clicked()
{
    timerInterval*=2;
    timerInterval=std::min(timerInterval,125);
    timer->setInterval(timerInterval);
    qDebug() << "Timer interval=" << timerInterval;
}

void MainWindow::on_faster_button_clicked()
{
    timerInterval/=2;
    timerInterval=std::max(timerInterval,1);
    timer->setInterval(timerInterval);
    qDebug() << "Timer interval=" << timerInterval;
}
