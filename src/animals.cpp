#include "animals.h"

using std::abs;
using std::atan2;
using std::sqrt;
using std::hypot;
using std::pair;
using std::make_pair;
using std::fmod;
using std::min;
using std::max;
using std::sin;
using std::cos;

const double epsilon = 0.00001;

std::default_random_engine generator;

double angleSum(double alpha, double beta) {
    return fmod(alpha+beta+3*M_PI,2*M_PI)-M_PI;
}

Animal::Animal(double _x, double _y, double _theta, double _v, double _max_v,
               double _vision_angle, double _vision_radius,
               double _alignment_weight,
               double _cohesion_weight, double _separation_weigh, double _minimal_distance,
               double _obstacle_weight, double _food_weight,
               double _enemy_weight,
               double _normal_v, double _sigma) :
    x(_x),
    y(_y),
    dx(_v*cos(_theta)),
    dy(_v*sin(_theta)),
    max_v(_max_v),
    vision_angle(_vision_angle),
    vision_radius(_vision_radius),
    alignment_weight(_alignment_weight),
    cohesion_weight(_cohesion_weight),
    separation_weight(_separation_weigh),
    minimal_distance(_minimal_distance),
    obstacle_weight(_obstacle_weight),
    food_weight(_food_weight),
    enemy_weight(_enemy_weight),
    max_acc(_normal_v),
    sigma(_sigma),
    distribution(0.0,sigma),
    foodnames(),
    predatornames()
{}

double Animal::getTheta() const {
    return angleBetween(dx+x,dy+y);
}

void Animal::limitVelocity() {
    if (hypot(dx,dy)>max_v) {
        dx=dx/hypot(dx,dy)*max_v;
        dy=dy/hypot(dx,dy)*max_v;
    }
}

void Animal::limitAcc(double prvV) {
    double diff = getV() - prvV;
    double normv;
    if (diff > max_acc) {
        normv = getV() - (diff - max_acc);
        dx=dx/hypot(dx,dy)*normv;
        dy=dy/hypot(dx,dy)*normv;
    }
    if (diff < -max_acc) {
        normv = getV() + (abs(diff) - max_acc);
        dx=dx/hypot(dx,dy)*normv;
        dy=dy/hypot(dx,dy)*normv;
    }
}

bool Animal::isInViewRadius(double px, double py) const {
    double l=hypot(px-x,py-y);
    if (l<vision_radius)
        return true;
    else
        return false;
}

bool Animal::isInMinimalRadius(double px, double py) const {
    double l=hypot(px-x,py-y);
    if (l<minimal_distance)
        return true;
    else
        return false;
}

double Animal::angleBetween(double px, double py) const {
    double X=px-x;
    if (abs(X)<epsilon) {
        if ((y-py)>epsilon) {
            return -M_PI;
        }
        else if ((y-py)<-epsilon){
            return M_PI;
        }
        else {
            return 0;
        }
    }
    else {
        return atan2(py-y,px-x);
    }
}

bool Animal::isInViewAngular(double angleBetween) const {
    return abs(fmod(angleBetween-getTheta()+M_PI,2*M_PI)-M_PI) < vision_angle/2;
}

bool Animal::isFood(string name) const {
    for (auto & it : foodnames) {
        if (it == name)
            return true;
    }
    return false;
}

bool Animal::isPredator(string name) const {
    for (auto & it : predatornames) {
        if (it == name)
            return true;
    }
    return false;
}

void Animal::actualize(const animalcollection &mytype, const obstacleCollection &obst, const animalcollection &victimtype, const animalcollection &predatortype, const animalcollection &othertype) {
    double prvV = getV();
    if (!mytype.empty()) {
        actualizeAlignment(mytype);
        actualizeCohesion(mytype);
        actualizeSeparation(mytype);
    }
    if (!othertype.empty()) {
        actualizeSeparation(othertype);
    }
    if (!victimtype.empty()) {
        actualizeFood(victimtype);
    }
    if (!predatortype.empty()) {
        actualizeEnemy(predatortype);
    }
    if (!obst.empty()) {
        actualizeObstacleInfluence(obst);
    }
    dx = 0.45*dx+max_v*cos(getTheta());
    dy = 0.45*dy+max_v*sin(getTheta());
    dx += 0.05*distribution(generator)*dx;
    dy += 0.05*distribution(generator)*dy;
    limitAcc(prvV);
    limitVelocity();
}

void Animal::actualizeAlignment(const animalcollection &vec) {
    double meandx=0.0;
    double meandy=0.0;
    for (auto & it : vec) {
        meandx+=it.second->getdX();
        meandy+=it.second->getdY();
    }
    meandx/=vec.size();
    meandy/=vec.size();
    dx += alignment_weight * (meandx - dx);
    dy += alignment_weight * (meandy - dy);
}

void Animal::actualizeCohesion(const animalcollection &vec) {
    double meanl=0.0;
    vector<double> l;
    for (auto & it : vec) {
        l.push_back(hypot(it.second->getX()-x,it.second->getY()-y));
        meanl+=hypot(it.second->getX()-x,it.second->getY()-y);
    }
    meanl/=l.size();
    double aktx=0.0;
    double akty=0.0;
    auto iter = vec.begin();
    for (unsigned int i=0;i<l.size();i++,iter++) {
        aktx += (iter->second->getX()-x)*(l[i]-meanl)/l[i];
        akty += (iter->second->getY()-y)*(l[i]-meanl)/l[i];
    }
    dx += cohesion_weight * aktx;
    dy += cohesion_weight * akty;
}

void Animal::actualizeSeparation(const animalcollection &vec) {
    double aktx=0.0;
    double akty=0.0;
    for (auto & it : vec) {
        aktx += (it.second->getX()-x)*minimal_distance/hypot(it.second->getX()-x,it.second->getY()-y)-(it.second->getX()-x);
        akty += (it.second->getY()-y)*minimal_distance/hypot(it.second->getX()-x,it.second->getY()-y)-(it.second->getY()-y);
    }
    dx -= separation_weight * aktx;
    dy -= separation_weight * akty;
}

void Animal::actualizeObstacleInfluence(const obstacleCollection &obst) {
    int howmany=101;
    double exitflag=false;
    double aktx=0.0;
    double akty=0.0;
    for (int i=0;i<howmany;i++) {
        for (int j=0;j<howmany;j++) {
            exitflag=false;
            Point p(x+cos(getTheta()+((double)i-((double)howmany-1.0)/2.0)/((double)howmany-1.0)*vision_angle)*vision_radius*0.5*j/(howmany-1),
                    y+sin(getTheta()+((double)i-((double)howmany-1.0)/2.0)/((double)howmany-1.0)*vision_angle)*vision_radius*0.5*j/(howmany-1));
            for (unsigned int k=0;k<obst.size();k++) {
                if (obst[k].isInside(p)) {
                    aktx += (p.getX()-x)*minimal_distance/hypot(p.getX()-x,p.getY()-y)-(p.getX()-x);
                    akty += (p.getY()-y)*minimal_distance/hypot(p.getX()-x,p.getY()-y)-(p.getY()-y);
                    exitflag=true;
                    break;
                }
            }
            if (exitflag) {
                break;
            }
        }
    }
    if (abs(akty) > epsilon && abs(aktx) > epsilon) {
        dx -= obstacle_weight * cos(angleBetween(aktx,akty)-getTheta()) * aktx;
        dy -= obstacle_weight * cos(angleBetween(aktx,akty)-getTheta()) * akty;
    }
}

void Animal::actualizeFood(const animalcollection &vec) {
    double aktx=0.0;
    double akty=0.0;
    double weig=10000.0;
    for (auto & it : vec) {
        if (weig > hypot(it.second->getX()-x,it.second->getY()-y)) {
            weig = hypot(it.second->getX()-x,it.second->getY()-y);
            aktx += (it.second->getX()-x)*vision_radius/hypot(it.second->getX()-x,it.second->getY()-y)-(it.second->getX()-x);
            akty += (it.second->getY()-y)*vision_radius/hypot(it.second->getX()-x,it.second->getY()-y)-(it.second->getY()-y);
        }
    }
    dx += food_weight * aktx;
    dy += food_weight * akty;
}

void Animal::actualizeEnemy(const animalcollection &vec) {
    double aktx=0.0;
    double akty=0.0;
    double weig=10000.0;
    for (auto & it : vec) {
        if (weig > hypot(it.second->getX()-x,it.second->getY()-y)) {
            weig = hypot(it.second->getX()-x,it.second->getY()-y);
            aktx += (it.second->getX()-x)*vision_radius/hypot(it.second->getX()-x,it.second->getY()-y)-(it.second->getX()-x);
            akty += (it.second->getY()-y)*vision_radius/hypot(it.second->getX()-x,it.second->getY()-y)-(it.second->getY()-y);
        }
    }
    dx -= enemy_weight * aktx;
    dy -= enemy_weight * akty;
}

std::map<std::string, double> Animal::getAttribute() const
{
    std::map<std::string, double> attribute;
    attribute["max_v"]=max_v;
    attribute["vision_angle"]=vision_angle;
    attribute["vision_radius"]=vision_radius;
    attribute["alignment_weight"]=alignment_weight;
    attribute["cohesion_weight"]=cohesion_weight;
    attribute["separation_weight"]=separation_weight;
    attribute["obstacle_weight"]=obstacle_weight;
    attribute["food_weight"]=food_weight;
    attribute["enemy_weight"]=enemy_weight;
    attribute["max_acc"]=max_acc;
    attribute["sigma"]=sigma;
    attribute["minimal_distance"]=minimal_distance;
    return attribute;
}

void Animal::setAttribute(std::map<std::string, double> attribute)
{
    max_v=attribute["max_v"];
    vision_angle=attribute["vision_angle"];
    vision_radius=attribute["vision_radius"];
    alignment_weight=attribute["alignment_weight"];
    cohesion_weight=attribute["cohesion_weight"];
    separation_weight=attribute["separation_weight"];
    obstacle_weight=attribute["obstacle_weight"];
    food_weight=attribute["food_weight"];
    enemy_weight=attribute["enemy_weight"];
    max_acc=attribute["max_acc"];
    sigma=attribute["sigma"];
    minimal_distance=attribute["minimal_distance"];
}

Velociraptor::Velociraptor(double _x, double _y, double _theta, double _v) :
Animal(_x,
_y,
_theta,
_v,
5.0,//max_v
M_PI*120/180,//vis_ang
250,//vis_rad
0.1,//ali
0.1,//coh
0.3,//sep
50,//min_dist
0.2,//obs
0.1,//food
0.1,//enemy
0.5,//max_acc
1.0//sigma
)
{
    foodnames.push_back(string("Lion"));
}

string Velociraptor::animalType() const {
    return string("Velociraptor");
}

Lion::Lion(double _x, double _y, double _theta, double _v) :
Animal(_x,
_y,
_theta,
_v,
5.0,//max_v
M_PI*120/180,//vis_ang
250,//vis_rad
0.1,//ali
0.1,//coh
0.3,//sep
50,//min_dist
0.4,//obs
0.4,//food
0.4,//enemy
0.5,//max_acc
1.0//sigma
)
{
    predatornames.push_back(string("Velociraptor"));
}

string Lion::animalType() const {
    return string("Lion");
}


Deer::Deer(double _x, double _y, double _theta, double _v):
    Animal(_x,
    _y,
    _theta,
    _v,
    6.0,//max_v
    M_PI*120/180,//vis_ang
    250,//vis_rad
    0.1,//ali
    0.1,//coh
    0.3,//sep
    50,//min_dist
    0.4,//obs
    0.4,//food
    0.4,//enemy
    0.5,//max_acc
    1.0//sigma
    )
    {
        predatornames.push_back(string("Velociraptor"));
        predatornames.push_back(string("Lion"));
}

std::string Deer::animalType() const
{
    return string("Deer");
}
