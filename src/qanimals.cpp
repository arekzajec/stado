#include "qanimals.h"
#include <QLinearGradient>
#include <QPainter>

#define M_PI 3.14159265

QAnimal::QAnimal(int size, QColor bodyColor, QColor eyeColor)
    : bodyColor(bodyColor),
      eyeColor(eyeColor),
      size(size){}

QRectF QAnimal::boundingRect() const
{
    return QRectF(-150, -150, 150, 150);
}

void QAnimal::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    (void)option;
    (void)widget;
    drawBody(painter);
    drawEye(painter);
}

void QAnimal::drawBody(QPainter *painter)
{
    painter->setBrush(bodyColor);
    painter->drawEllipse(QPoint(0,0),size,size);
}

void QAnimal::drawEye(QPainter *painter)
{
    painter->setBrush(eyeColor);
    painter->setPen(Qt::black);
    painter->drawEllipse(QPoint(0.7*size,0.7*size),5,5);
    painter->drawEllipse(QPoint(0.7*size,-0.7*size),5,5);
    painter->setBrush(Qt::black);
    painter->drawEllipse(QPoint(0.7*size,0.7*size),2,2);
    painter->drawEllipse(QPoint(0.7*size,-0.7*size),2,2);
}

QAnimalView::QAnimalView(int size, QColor viewColor, int viewAngle, int viewRange)
    : size(size),
      viewColor(viewColor),
      viewAngle(viewAngle),
      viewRange(viewRange){}

QRectF QAnimalView::boundingRect() const
{
    return QRectF(-150, -150, 150, 150);
}

void QAnimalView::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    (void)option;
    (void)widget;
    drawView(painter);
}

void QAnimalView::setViewRad(double Angle, double Range)
{
    viewAngle=Angle*180/M_PI;
    viewRange=Range;
}

void QAnimalView::drawView(QPainter *painter)
{
    painter->setBrush(viewColor);
    painter->setPen(Qt::NoPen);
    painter->drawPie(-(viewRange),-(viewRange),
                     2*(viewRange),2*(viewRange),-viewAngle*8,viewAngle*16);
}
