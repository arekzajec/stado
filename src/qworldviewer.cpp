#include "inc/qworldviewer.h"
#include <QDebug>
#include <QtAlgorithms>
#include <QColor>

QWorldViewer::QWorldViewer(QWidget *parent, const WorldInterface &world, std::shared_ptr<Observer> observer):
    scene(new QWorldScene(parent)), world(world), observer(observer), qanimals(), qanimalsview(), obstacles(), idUpdate(),idReload()
{
    scene->setSceneRect(0,0,500,500);
    idUpdate = observer->subscribeUpdate(std::bind(&QWorldViewer::updateScene,this));
    idReload = observer->subscribeReload(std::bind(&QWorldViewer::reloadAnimals,this));
}

QWorldViewer::~QWorldViewer()
{
    observer->unsubscribeUpdate(idUpdate);
    observer->unsubscribeReload(idReload);

}

QGraphicsScene *QWorldViewer::getScene()
{
    return scene;
}

QWorldScene::QWorldScene(QWidget *&parent): QGraphicsScene(parent), pressed(false), oldMousePos(){}

void QWorldScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    pressed=true;
    oldMousePos=event->scenePos();
}

void QWorldScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if(!pressed)
        return;

    float posx,posy,x,y,w,h;
    posx=sceneRect().x();
    posy=sceneRect().y();

    w=sceneRect().width();
    h=sceneRect().height();
    x=oldMousePos.x()-event->scenePos().x()+posx;
    y=oldMousePos.y()-event->scenePos().y()+posy;

    setSceneRect(x,y,w,h);
}

void QWorldScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *)
{
    pressed=false;
}

void QWorldViewer::updateScene()
{
    newObstacles(world.getObstacles());
    updateAnimals();
}

void QWorldViewer::updateAnimals()
{
    for(auto &animal : world.getAnimals())
    {
        int index = animal.first;
        //if(index==1)
            //qDebug() << animal.second->getX();
        if(qanimals.find(index) == qanimals.end())
            addAnimal(animal);
        qanimals[index]->setPos(animal.second->getX(),
                                animal.second->getY());
        qanimals[index]->setRotation(animal.second->getTheta()*180/M_PI);
        qanimalsview[index]->setPos(animal.second->getX(),
                                    animal.second->getY());
        qanimalsview[index]->setRotation(animal.second->getTheta()*180/M_PI);
        qanimalsview[index]->setViewRad(animal.second->getVisionAngle()
                                        , animal.second->getVisionRadius());
    }

}

void QWorldViewer::addAnimal(const std::pair<const int,std::unique_ptr<Animal>>&animal)
{

    qanimalsview[animal.first]=new QAnimalView(10,QColor::fromRgbF(0,0,1,0.1),90,90);
    if(animal.second->animalType()=="Velociraptor")
        qanimals[animal.first]=new QVelociraptor();
    else if(animal.second->animalType()=="Lion")
        qanimals[animal.first]=new QLion();
    else if(animal.second->animalType()=="Deer")
        qanimals[animal.first]=new QDeer();
    else
        qDebug() << "Nie zaimplementowano " << animal.second->animalType().c_str();
    updateAnimalItems();
}

void QWorldViewer::newObstacles(const ObstacleContainer &_obstacles) {
    obstacles = _obstacles;
}

void QWorldViewer::updateAnimalItems()
{
    for(auto item : scene->items())
    {
        scene->removeItem(item);
    }

    for(auto qanimalview : qanimalsview)
    {
        scene->addItem(qanimalview.second);
    }

    for(auto qanimal : qanimals)
    {
        scene->addItem(qanimal.second);
    }

    updateObstacleItems();

}

void QWorldViewer::refreshAnimalItems()
{
    updateAnimals();
}

void QWorldViewer::updateObstacleItems()
{
    QPolygon *polygon;
    for(auto obstacle : obstacles.getObstacles())
    {
        polygon = new QPolygon();
        for(auto point : obstacle.getVertices())
            (*polygon) << QPoint(point.getX(),point.getY());
        scene->addPolygon(*polygon,QPen(Qt::darkGreen),Qt::darkGreen);
    }

}

void QWorldViewer::reloadAnimals()
{
    qanimals.clear();
    qanimalsview.clear();
    updateAnimals();
    updateAnimalItems();
}
